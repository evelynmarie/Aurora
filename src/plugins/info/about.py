# Copyright (C) 2016-2022 Evelyn Marie and contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

from telegram import Update
from telegram.constants import ChatAction, ParseMode
from telegram.ext import ContextTypes

from constants import name, version


async def about(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """
    Sends a message giving info about the bot and linking
    to the bot's source code on GitHub.
    """
    bot = context.bot.first_name
    await update.message.chat.send_action(action=ChatAction.TYPING)
    await update.message.reply_text(
        parse_mode=ParseMode.MARKDOWN,
        disable_web_page_preview=True,
        text=f"*{bot}* is powered by *{name}* {version}, a plugin-based bot. "
        "It is written in the Python programming language. You can view which "
        "libraries we use by sending the /libraries command.\n\n",
    )
