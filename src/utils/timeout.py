#
# Copyright (C) 2016-2022 Evelyn Marie and contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

import time
from functools import wraps

timeout_funcs = {}


def get_seconds():
    return int(round(time.time()))


def timeout(duration: int):
    """
    Allows commands to have a timeout period after
    they have been used in order to stop problems
    relating to command abuse from happening.

    :param duration: Specified duration of how long the
    timeout should last. This parameter only supports
    durations in seconds.
    """

    def check(func):
        @wraps(func)
        async def inner(*args, **kwargs):
            if func not in timeout_funcs or timeout_funcs[func] < get_seconds():
                timeout_funcs[func] = get_seconds() + duration
                return func(*args, **kwargs)

            cmd = func.__name__

            try:
                update = args[1]
                msg = f"/{cmd} is timed out for {duration} seconds. Please wait the specified amount of time before attempting to use the command again."
                await update.message.reply_text(msg)
            except (IndexError, AttributeError):
                msg = (
                    "Check if you spelled the attributes right and if this is applied "
                    "on the right function. If it isn't, correct the attribute names "
                    "and try calling this again. And make sure you're applying this "
                    "to the right function!"
                )
                raise Exception(msg)

        return inner

    return check
