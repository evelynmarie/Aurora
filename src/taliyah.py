#!/usr/bin/env python
#
# Copyright (C) 2016-2022 Evelyn Marie and contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

"""
A bot written in Python for the Telegram chat platform.
"""

import configparser
import logging
import os
import platform
import sys

from telegram import Update
from telegram.ext import ApplicationBuilder, CommandHandler, ContextTypes

from constants import name, revision, version
from plugins.fun.xkcd import xkcd as xkcd
from plugins.info.about import about
from plugins.info.libraries import libraries
from plugins.misc.kernel import kernel
from plugins.misc.start import start
from plugins.misc.user import id, me
from plugins.util.system import system

# Enforce the minimum Python version used to 3.8 or greater, as Taliyah
# may begin using newer Python features down the line. This also allows
# Taliyah to work on some older distributions that don't have the newest
# Python, like Ubuntu 20.04 LTS.
if sys.version_info < (3, 8):
    print("Python 3.8 or later is required. Please update your Python version.")
    sys.exit(1)

# Setup a basic logging framework.
logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO, datefmt="%m/%d/%Y %I:%M:%S %p")

logger = logging.getLogger(name="Taliyah")


def main() -> None:
    """Main entry point.

    This method sets up logging and the configuration system, creates an Application
    instance, and registers all application handlers, such as commands and error
    handlers.
    """

    logger.info(f"{name} {version} (rev: {revision}) on Python {platform.python_version()} starting.")

    # Initialize the configuration system
    config = configparser.ConfigParser(allow_no_value=True)
    config["Basic Settings"] = {"token": ""}
    config["Spotipy Settings"] = {"client_id": "", "client_secret": ""}

    # Generate the config file as config.ini. If the file already exists,
    # do not overwrite it, as we don't want the user to lose their existing
    # configuration.
    if not os.path.exists("./config.ini"):
        with open(file="./config.ini", mode="x", encoding="utf-8") as c:
            config.write(c)
            logger.info("Config file generated. Before starting the bot, please add your bot token to the file.")

        c.close()
        sys.exit(1)
    else:
        logger.info("Configuration file found.")

    config.read("config.ini")

    def error(update: Update, context: ContextTypes.DEFAULT_TYPE):
        """Update error handler

        Logs a warning to stdout (the terminal) when an update happens to
        cause an error.
        """
        logger.warning(f"Update {update.update_id} caused error: {context.error}")

    # initialize application and register all handlers, including all command handlers and
    # any error handlers.
    application = ApplicationBuilder().token(config["Basic Settings"]["token"]).build()
    application.add_handler(CommandHandler("xkcd", xkcd))
    application.add_handler(CommandHandler("about", about))
    application.add_handler(CommandHandler("start", start))
    application.add_handler(CommandHandler("libraries", libraries))
    application.add_handler(CommandHandler("me", me))
    application.add_handler(CommandHandler("linux", kernel))
    application.add_handler(CommandHandler("id", id))
    application.add_handler(CommandHandler("system", system))
    application.add_error_handler(error)

    logger.info(f"{name} {version} started.")

    # Start polling for updates
    application.run_polling()


if __name__ == "__main__":
    main()
